package veterinaire.client;

import veterinaire.share.*;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client extends UnicastRemoteObject implements IClient{
	private Client() throws RemoteException{}

    @Override
    public void alert(String msg) throws RemoteException {
        System.out.println(msg);
    }

	public static void main(String[] args) {

		String host = (args.length < 1) ? null : args[0];
		try {
			Registry registry = LocateRegistry.getRegistry(host);
			IOffice stub = (IOffice) registry.lookup("Veto");

            IClient me = new Client();
            stub.connect(me); // je me connecte

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        stub.logout(me);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            });
			IAnimal wandle = stub.findPatientByName("Wandle");
			if(wandle != null) {
                System.out.println("Recherche de Wandle: " + wandle.getInfos());
            }
			stub.setMedicalFile(wandle,"Mort");
			String infos = stub.getInfos();
			System.out.println("infos: " + infos);
			System.out.println("*******************************************");
			stub.addPatient(new Animal("Bill", "Boule", new SubSpecies("Chien", 13, "azeaze"), "Labrador", new MedicalFile("En pleine forme")));

			stub.foo(new SubSpecies("Chien", 13, "azeaze")); //test de foo avec une sous-classe de Species
		} catch (Exception e) {
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}

}
