package veterinaire.share;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Animal extends UnicastRemoteObject implements IAnimal {

    String name;
    String master;
    Species species;
    String race;
    MedicalFile dossier;


    public Animal(String name, String master, Species species, String race, MedicalFile dossier) throws RemoteException {
        super();
        this.name = name;
        this.master = master;
        this.species = species;
        this.race = race;
        this.dossier = dossier;
    }


    public String getInfos() throws RemoteException {
        return name + ", Maitre: " + master + ", Species: " + species.toString() + ", Race: " + race + ", Dossier de suivi : " + dossier.getInfos() + "\n";
    }

    @Override
    public void setMedicalFile(String state) throws RemoteException {
        this.dossier.state = state;
    }

    @Override
    public String getName() throws RemoteException {
        return name;
    }

    @Override
    public void setName(String name) throws RemoteException {
        this.name = name;
    }

    @Override
    public String getMaster() throws RemoteException {
        return master;
    }

    @Override
    public void setMaster(String master) throws RemoteException {
        this.master = master;
    }

    @Override
    public Species getSpecies() throws RemoteException {
        return species;
    }

    @Override
    public String getRace() throws RemoteException {
        return race;
    }

    @Override
    public void setRace(String race) throws RemoteException {
        this.race = race;
    }

    @Override
    public IMedicalFile getMedicalFile() throws RemoteException {
        return dossier;
    }
}
