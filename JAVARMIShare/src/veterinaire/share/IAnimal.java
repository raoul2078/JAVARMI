package veterinaire.share;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IAnimal extends Remote {

    public String getInfos() throws RemoteException;

    public String getName() throws RemoteException;

    public void setName(String name) throws RemoteException;

    public String getMaster() throws RemoteException;

    public void setMaster(String master) throws RemoteException;

    public String getRace() throws RemoteException;

    public void setRace(String race) throws RemoteException;

    public IMedicalFile getMedicalFile() throws RemoteException;

    public void setMedicalFile(String state) throws RemoteException;

    public Species getSpecies() throws RemoteException;

}
