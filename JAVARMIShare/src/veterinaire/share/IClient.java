package veterinaire.share;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClient extends Remote {
    public void alert(String msg) throws RemoteException;
}
