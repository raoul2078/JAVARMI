package veterinaire.share;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MedicalFile extends UnicastRemoteObject implements IMedicalFile {
    String state;

    public MedicalFile(String state) throws RemoteException {
        this.state = state;
    }

    @Override
    public String getInfos() throws RemoteException {
        return state;
    }
}
