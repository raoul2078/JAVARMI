package veterinaire.share;

import java.io.Serializable;

public class Species implements Serializable {


    private static final long serialVersionUID = 2372845476313301932L;
    String name;
    int lifeExpectancy;

    public Species(String name, int lifeExpectancy) {
        this.name = name;
        this.lifeExpectancy = lifeExpectancy;
    }

    @Override
    public String toString() {
        return name + ", esp. de vie: " + lifeExpectancy + " ans";
    }
}
